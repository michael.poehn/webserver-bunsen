# Rules to rewrite URLs of the old EZ Fellowship site
#
# This file assumes  "RewriteEngine On"

############ Fellows pages
#
# The order of these rules is important! Specific rules first!

# Some special pages in /fellows
RewriteRule  ^(/[a-z]{2})*/fellows/meetings$        http://wiki.fsfe.org/MeetingHowTo      [R=permanent,L]
RewriteRule  ^(/[a-z]{2})*(/fellows)*/refund$       http://wiki.fsfe.org/Refund            [R=permanent,L]

# Fellow blogs
Include /etc/apache2/rewrite_blogs.conf

# Other Fellows homepage content
# (we redirect to the Fellow wiki user pages, since we don't know if the subdir tree is preserved)
RewriteRule  ^(/[a-z]{2})*/[Ff]ellows/([^/]+)    http://wiki.fsfe.org/Fellows/$2  [R=permanent,L]

# The "Fellows" page
RewriteRule  ^(/[a-z]{2})*/[Ff]ellows(/*)$           http://wiki.fsfe.org/Fellows     [R=permanent,L]


############ Other rules moved from the EZ vhost
    
# redirect pruned Plone paths to Fellow home directories
Rewriterule ^/[Mm]embers/(.*) http://wiki.fsfe.org/Fellows/$1 [R=permanent,L]

RewriteRule ^(/[a-z]{2})*/planet$                     http://planet.fsfe.org [R=permanent,L]
RewriteRule ^(/[a-z]{2})*/fellowship_blog_overview$   http://planet.fsfe.org [R=permanent,L]



############ Other pages
 
# Login
#RewriteRule ^(/[a-z]{2})*(/user)*/[Ll]ogin   https://fellowship.fsfe.org/login    [R=permanent,L]
RewriteRule ^(/[a-z]{2})*/fsfeuser$           https://fellowship.fsfe.org/login    [R=permanent,L]

# Join
RewriteRule ^(/[a-z]{2})*/[Jj]oin$             https://fellowship.fsfe.org/join    [R=permanent,L]
RewriteRule ^(/[a-z]{2})*/[Jj]oin/             https://fellowship.fsfe.org/join    [R=permanent,L]
RewriteRule ^(/[a-z]{2})*/[Jj]oin_us$          https://fellowship.fsfe.org/join    [R=permanent,L]
RewriteRule ^(/[a-z]{2})*/[Jj]oin_us/          https://fellowship.fsfe.org/join    [R=permanent,L]
RewriteRule ^(/[a-z]{2})*/fsfeuser/register$   https://fellowship.fsfe.org/join    [R=permanent,L]

# Paypal
RewriteRule ^/paypal/(.*) https://fellowship.fsfe.org/login/paypal_manual.php?paypalid=$1 [R=permanent,L]



# EZ right menu
#
# Note: "about" and "contact" omitted, since they clash with the corresponding
# pages on the FSFE site
RewriteRule ^(/[a-z]{2})*/forums$          http://fellowship.fsfe.org/contact.html [R=permanent,L]
RewriteRule ^(/[a-z]{2})*/card$            http://wiki.fsfe.org/Crypto_Card        [R=permanent,L]
RewriteRule ^(/[a-z]{2})*/card/            http://wiki.fsfe.org/Crypto_Card        [R=permanent,L]
RewriteRule ^(/[a-z]{2})*/advocacy$        http://wiki.fsfe.org/Advocacy           [R=permanent,L]
RewriteRule ^(/[a-z]{2})*/FAQ$             http://wiki.fsfe.org/FAQ                [R=permanent,L]
RewriteRule ^(/[a-z]{2})*/fun$             http://wiki.fsfe.org/Fun                [R=permanent,L]
RewriteRule ^(/[a-z]{2})*/fun/             http://wiki.fsfe.org/Fun                [R=permanent,L]


# Other special pages
RewriteRule ^(/[a-z]{2})*/interviews(.*) http://blogs.fsfe.org/fellowship-interviews$2 [R=permanent,L]
RewriteRule ^(/[a-z]{2})*/fellowship-interviews.rss$ http://blogs.fsfe.org/fellowship-interviews/feed [R=permanent,L]
RewriteRule ^(/[a-z]{2})*/[Tt]ranscripts$         http://wiki.fsfe.org/Transcripts [R=permanent,L]

# Planet
RewriteRule ^(/[a-z]{2})*/rss/feed/planet_rss20$ http://planet.fsfe.org/en/rss20.xml [R=permanent,L]
RewriteRule ^(/[a-z]{2})*/blogs_aggregation$ http://planet.fsfe.org/ [R=permanent,L]

# News
#
# The main "news" page is not rewritten, since it clashes with the corresponding page on the FSFE site
#
# We only keep the old Fellowship News feed...
RewriteRule ^(/[a-z]{2})*/rss/feed/news_rss20$     http://blogs.fsfe.org/feed/     [R=permanent,L]
# ... and the single news articles (see rewrite_blogs.conf and ./maps/blog_news.txt)


# Events:
#
# Note: "events" omitted, since it clashes with the corresponding page on the FSFE site
# We only keep the old Fellowship News feed
RewriteRule ^(/[a-z]{2})*/rss/feed/events_rss20$    http://wiki.fsfe.org/FellowshipEvents?action=EventAggregatorSummary&doit=1&category=CategoryFellowshipEvents&format=RSS     [R=permanent,L]


# TODO: we still don't know where to point these
#RewriteRule ^(/[a-z]{2})*/supporters                ???/     [R=permanent,L]



