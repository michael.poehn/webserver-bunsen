<VirtualHost *:443>

    #
    # HTTP host config
    #

    ServerName freeyourandroid.org
    ServerAlias www.freeyourandroid.org
    ServerAdmin webmaster@fsfe.org

    # Dummy directory
    DocumentRoot /var/www/html

    ErrorLog /var/log/apache2/freeyourandroid-error.log
    CustomLog /var/log/apache2/freeyourandroid-access.log combined

    #
    # TLS config
    #

    SSLEngine on
    SSLCertificateFile /etc/letsencrypt/live/freeyourandroid.org/cert.pem
    SSLCertificateKeyFile /etc/letsencrypt/live/freeyourandroid.org/privkey.pem
    SSLCertificateChainFile /etc/letsencrypt/live/freeyourandroid.org/chain.pem

    # TODO: obsolete, needs replacement?
    # SSLCACertificateFile /etc/ssl/certs/StartSSL_root.pem
    SetEnvIf User-Agent ".*MSIE.*" nokeepalive ssl-unclean-shutdown

    SSLProtocol             all -SSLv2 -SSLv3
    SSLHonorCipherOrder     on
    SSLCipherSuite          ECDHE-RSA-AES128-GCM-SHA256:ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-AES256-GCM-SHA384:DHE-RSA-AES128-GCM-SHA256:DHE-DSS-AES128-GCM-SHA256:kEDH+AESGCM:ECDHE-RSA-AES128-SHA256:ECDHE-ECDSA-AES128-SHA256:ECDHE-RSA-AES128-SHA:ECDHE-ECDSA-AES128-SHA:ECDHE-RSA-AES256-SHA384:ECDHE-ECDSA-AES256-SHA384:ECDHE-RSA-AES256-SHA:ECDHE-ECDSA-AES256-SHA:DHE-RSA-AES128-SHA256:DHE-RSA-AES128-SHA:DHE-DSS-AES128-SHA256:DHE-RSA-AES256-SHA256:DHE-DSS-AES256-SHA:DHE-RSA-AES256-SHA:ECDHE-RSA-DES-CBC3-SHA:ECDHE-ECDSA-DES-CBC3-SHA:AES128-GCM-SHA256:AES256-GCM-SHA384:AES128-SHA256:AES256-SHA256:AES128-SHA:AES256-SHA:AES:CAMELLIA:DES-CBC3-SHA:!aNULL:!eNULL:!EXPORT:!DES:!RC4:!MD5:!PSK:!aECDH:!EDH-DSS-DES-CBC3-SHA:!EDH-RSA-DES-CBC3-SHA:!KRB5-DES-CBC3-SHA

    #
    # security headers
    # TODO: make as restrictive as possible
    #

    Header always set Strict-Transport-Security "max-age=15768000"

    #Prevent attack described on https://httpoxy.org/
    RequestHeader unset Proxy early

    # Rewrite whole domain to fsfe.org subsite
    RewriteEngine on
    RewriteRule ^(.*) %{REQUEST_SCHEME}://fsfe.org/campaigns/android/ [L,R=permanent]

</VirtualHost>

# rewrite all HTTP requests to HTTPS

<VirtualHost *:80>

    ServerName freeyourandroid.org
    ServerAlias www.freeyourandroid.org
    ServerAdmin webmaster@fsfe.org

    RewriteEngine On
    RewriteRule (.*) https://%{SERVER_NAME}$1 [R,L]

</VirtualHost>
