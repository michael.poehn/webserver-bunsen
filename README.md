

## dev docs

### prepare for development/deployment

#### ssh config

This is the config-snippet has to be added to your __~/.ssh/config__.

    Host bunsen.fsfeurope.org
        Hostname bunsen.fsfeurope.org
        IdentityFile ~/.ssh/id_rsa
        User root
        UserKnownHostsFile ~/.ssh/known_hosts_fsfeurope

### run/update webserver deployment

    ansible-playbook -i bunsen.fsfeurope.org, bunsenApache2Deploy.yaml
